﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureApp.Models
{
    class StraightPyramid : Figure
    {
        public override double Height { get; set; }
        public override double Ratio { get; set; }
        public double Perimeter { get; set; }
        public double Apothem { get; set; }

        public StraightPyramid(double height, double ratio, double perimeter, double apothem)
        {
            Height = height;
            Ratio = ratio;
            Perimeter = perimeter;
            Apothem = apothem;
        }

        public override double Area()
        {
            return (1.0 / 2.0) * Perimeter * (Ratio + Apothem);
        }

        public override double Volume()
        {
            return (1.0 / 3.0) * BaseArea() * Height;
        }

        public double BaseArea()
        {
            return (Perimeter * Ratio) / 2;
        }
    }
}
