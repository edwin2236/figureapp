﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureApp.Models
{
    class Cylinder : Figure
    {
        public override double Height { get; set; }
        public override double Ratio { get; set; }

        public Cylinder(double height, double ratio)
        {
            Height = height;
            Ratio = ratio;
        }

        public override double Area()
        {
            return 2 * Math.PI * Math.Pow(Ratio, 2) * (Height + Math.Pow(Ratio, 2));
        }

        public override double Volume()
        {
            return Math.PI * Math.Pow(Math.Pow(Ratio, 2), 2) * Height;
        }
    }
}
