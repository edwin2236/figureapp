﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureApp.Models
{
    class Cone : Figure
    {
        public override double Height { get; set; }
        public override double Ratio { get; set; }
        public double Thickness { get; set; }

        public Cone(double height, double ratio, double thickness)
        {
            Height = height;
            Ratio = ratio;
            Thickness = thickness;
        }

        public override double Area()
        {
            return Math.PI * Math.Pow(Ratio, 2) * (Thickness + Math.Pow(Ratio, 2));
        }

        public override double Volume()
        {
            return (1 / 3) * Math.PI * Math.Pow(Math.Pow(Ratio, 2), 2) * Height;
        }
    }
}
