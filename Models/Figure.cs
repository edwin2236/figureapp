﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureApp.Models
{
    abstract class Figure
    {
        public abstract double Height { get; set; }
        public abstract double Ratio { get; set; }

        public abstract double Area();
        public abstract double Volume();
    }
}
