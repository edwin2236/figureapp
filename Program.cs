﻿using System;
using FigureApp.Models;

namespace FigureApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string option;
            bool isValidOption = true;
            Console.WriteLine("================================================================================");
            Console.WriteLine("================================== Figure App ==================================");
            Console.WriteLine("================================================================================");
            Console.WriteLine("Autor: Esteban Becerra\n\n");
            Console.WriteLine("Información: La aplicación Figure App solicitará acontinuación los datos necesarios\npara calcular el área y el volúmen de las siguientes figuras:");
            Console.WriteLine("     Cilindro, Cono, Pirámide recta\n.");
            Console.WriteLine("Seleccione una opción de las que se muestra a continuación.");
            Console.WriteLine("1. Calcular área y volúmen de un Cilidro.");
            Console.WriteLine("2. Calcular área y volúmen de un Cono.");
            Console.WriteLine("3. Calcular área y volúmen de una Pirámide recta.");
            Console.WriteLine("4. Salir.\n");
            do
            {
                if (!isValidOption)
                {
                    Console.Clear();
                    Console.WriteLine("================================================================================");
                    Console.WriteLine("================================== Figure App ==================================");
                    Console.WriteLine("================================================================================");
                    Console.WriteLine("Autor: Esteban Becerra\n\n");
                    Console.WriteLine("Información: La aplicación Figure App solicitará acontinuación los datos necesarios\npara calcular el área y el volúmen de las siguientes figuras:");
                    Console.WriteLine("     Cilindro, Cono, Pirámide recta\n.");
                    Console.WriteLine("Seleccione una opción de las que se muestra a continuación.");
                    Console.WriteLine("1. Calcular área y volúmen de un Cilidro.");
                    Console.WriteLine("2. Calcular área y volúmen de un Cono.");
                    Console.WriteLine("3. Calcular área y volúmen de una Pirámide recta.");
                    Console.WriteLine("4. Salir.\n");
                }

                if (isValidOption)
                {
                    Console.WriteLine("Opción:");
                }
                else
                {
                    Console.WriteLine("No es una opción válida, ingrese una opción valida");
                }
                option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        MenuItemCylinder();
                        isValidOption = true;
                        break;
                    case "2":
                        MenuItemCone();
                        isValidOption = true;
                        break;
                    case "3":
                        MenuItemStraightPyramid();
                        isValidOption = true;
                        break;
                    case "4":
                        break;
                    default:
                        isValidOption = false;
                        break;
                }
            } while (!option.Trim().Equals("4"));
            //Console.ReadKey();
            Environment.Exit(1);
        }

        private static void MenuItemStraightPyramid()
        {
            string Height, Radio, Perimeter, Apothem;
            double HeightParse, RadioParse, PerimeterParse, ApothemParse;
            Console.WriteLine("Ingrese el valor de la altura.");
            Height = Console.ReadLine();
            Console.WriteLine("Ingrese el valor del a' (radio).");
            Radio = Console.ReadLine();
            Console.WriteLine("Ingrese el valor del apotema");
            Apothem = Console.ReadLine();
            Console.WriteLine("Ingrese el valor del perímetro");
            Perimeter = Console.ReadLine();
            HeightParse = double.Parse(Height);
            RadioParse = double.Parse(Radio);
            PerimeterParse = double.Parse(Perimeter);
            ApothemParse = double.Parse(Apothem);
            var straightPyramid = new StraightPyramid(HeightParse, RadioParse, PerimeterParse, ApothemParse);
            Console.WriteLine("=================================================");
            Console.WriteLine("Pirámide recta...");
            Console.WriteLine("Altura: " + straightPyramid.Height + "\na' (radio): " + straightPyramid.Ratio + "\nApotema:" + straightPyramid.Apothem + "\nPerímetro:" +
                straightPyramid.Perimeter + "\nÁrea base: " + straightPyramid.BaseArea() + "\nÁrea: " + straightPyramid.Area() + "\nVolúmen: " + straightPyramid.Volume());
            Console.WriteLine("=================================================\n");
            Console.WriteLine("Seleccione otra opción");
        }

        private static void MenuItemCone()
        {
            string Height, Radio, Thickness;
            double HeightParse, RadioParse, ThicknessParse;
            Console.WriteLine("Ingrese el valor de la altura.");
            Height = Console.ReadLine();
            Console.WriteLine("Ingrese el valor de la radio.");
            Radio = Console.ReadLine();
            Console.WriteLine("Ingrese el valor del grosor del cono.");
            Thickness = Console.ReadLine();
            HeightParse = double.Parse(Height);
            RadioParse = double.Parse(Radio);
            ThicknessParse = double.Parse(Thickness);
            var cone = new Cone(HeightParse, RadioParse, ThicknessParse);
            Console.WriteLine("=================================================");
            Console.WriteLine("Cono...");
            Console.WriteLine("Altura: " + cone.Height + "\nRadio: " + cone.Ratio + "\nGrosor:" + cone.Thickness + "\nÁrea: " + cone.Area() + "\nVolúmen: " + cone.Volume());
            Console.WriteLine("=================================================\n");
            Console.WriteLine("Seleccione otra opción");
        }

        private static void MenuItemCylinder()
        {
            string Height, Radio;
            double HeightParse, RadioParse;
            Console.WriteLine("Ingrese el valor de la altura.");
            Height = Console.ReadLine();
            Console.WriteLine("Ingrese el valor de la radio.");
            Radio = Console.ReadLine();
            HeightParse = double.Parse(Height);
            RadioParse = double.Parse(Radio);
            var cylinder = new Cylinder(HeightParse, RadioParse);
            Console.WriteLine("=================================================");
            Console.WriteLine("Cilindro...");
            Console.WriteLine("Altura: " + cylinder.Height + "\nRadio: " + cylinder.Ratio + "\nÁrea: " + cylinder.Area() + "\nVolúmen: " + cylinder.Volume());
            Console.WriteLine("=================================================\n");
            Console.WriteLine("Seleccione otra opción");
        }
    }
}
